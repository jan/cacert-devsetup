FROM debian:jessie

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
    apt-get install -y --no-install-recommends \
    ca-certificates \
    curl \
    gettext \
    libapache2-mod-php5 \
    locales-all \
    mariadb-client \
    make \
    netbase \
    nullmailer \
    openssl-blacklist \
    openssl-blacklist-extra \
    php-apc \
    php-fpdf \
    php-gettext \
    php-mail \
    php-mail-mime \
    php-mail-mimedecode \
    php-net-smtp \
    php-net-socket \
    php-tcpdf \
    php5-apcu \
    php5-curl \
    php5-gd \
    php5-imagick \
    php5-imap \
    php5-json \
    php5-mcrypt \
    php5-mysql \
    php5-pspell \
    php5-recode \
    psmisc \
    wamerican \
    whois \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && curl --silent --location --output /usr/local/bin/dumb-init \
       https://github.com/Yelp/dumb-init/releases/download/v1.2.4/dumb-init_1.2.4_x86_64 \
    && chmod +x /usr/local/bin/dumb-init

COPY docker/apache-webdb-foreground /usr/local/bin/
COPY testca/root/ca.crt.pem /usr/local/share/ca-certificates/testca_root.crt
COPY testca/class3/ca.crt.pem /usr/local/share/ca-certificates/testca_class3.crt
COPY testca/certs/www.cacert.localhost.crt.pem /etc/apache2/ssl/certs/
COPY testca/certs/www.cacert.localhost.key.pem /etc/apache2/ssl/private/
COPY testca/certs/cachain.crt.pem /etc/ssl/apache2/certs/combined.crt

COPY docker/apache-webdb-virtualhost.conf /etc/apache2/sites-available/www.cacert.localhost.conf
COPY docker/cacert.conf /etc/apache2/conf-available/
COPY docker/php5-cacert.ini /etc/php5/mods-available/cacert.ini
COPY docker/feed.rss /usr/local/etc/application/feed.rss

VOLUME /www
VOLUME /certs

RUN a2ensite www.cacert.localhost ; \
    a2dissite 000-default ; \
    a2enconf cacert ; \
    a2enmod headers ; \
    a2enmod rewrite ; \
    a2enmod ssl ; \
    ln -s /etc/php5/mods-available/cacert.ini /etc/php5/apache2/conf.d/20-cacert.ini ; \
    cd /usr/local/share/ca-certificates ; \
    curl --silent --remote-name http://www.cacert.org/certs/root_X0F.crt ; \
    curl --silent --remote-name http://www.cacert.org/certs/class3_X0E.crt ; \
    update-ca-certificates

EXPOSE 80
EXPOSE 443

ENTRYPOINT ["/usr/local/bin/dumb-init", "--"]
CMD ["/usr/local/bin/apache-webdb-foreground"]
