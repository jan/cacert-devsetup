FROM debian:jessie

COPY testca/root/ca.crt.pem /usr/local/share/ca-certificates/testca_root.crt
COPY testca/class3/ca.crt.pem /usr/local/share/ca-certificates/testca_class3.crt

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
    apt-get install -y --no-install-recommends \
    ca-certificates \
    gnupg \
    libdbd-mysql-perl \
    libdbi-perl \
    libdevice-serialport-perl \
    libemail-mime-perl \
    libfile-counterfile-perl \
    libreadonly-perl \
    openssl \
    perl \
    socat \
    xdelta \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

VOLUME /srv/certs

COPY cacert-software/CommModule/client.pl \
     cacert-software/CommModule/logclean.sh \
     /srv/CommModule/
COPY docker/run-signer_client usr/local/bin/

WORKDIR /srv/CommModule

CMD ["run-signer_client"]
