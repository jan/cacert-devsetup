FROM debian:jessie

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
    apt-get install -y --no-install-recommends \
    gnupg \
    libdevice-serialport-perl \
    libdigest-sha-perl \
    libfile-counterfile-perl \
    libreadonly-perl \
    openssl \
    perl \
    socat \
    xdelta \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY cacert-software/CommModule/server.pl \
     cacert-software/CommModule/logclean.sh \
     /srv/CommModule/
COPY docker/run-signer usr/local/bin/
COPY docker/signer-config/* /srv/caconfig/

COPY testca /srv/testca/

VOLUME /srv/ca

CMD ["/usr/local/bin/run-signer"]
