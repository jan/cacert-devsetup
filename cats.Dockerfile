FROM debian:jessie

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
    apt-get install -y --no-install-recommends \
    ca-certificates \
    curl \
    gettext \
    libapache2-mod-php5 \
    locales-all \
    nullmailer \
    php5-mysql \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && curl --silent --location --output /usr/local/bin/dumb-init \
       https://github.com/Yelp/dumb-init/releases/download/v1.2.4/dumb-init_1.2.4_x86_64 \
    && chmod +x /usr/local/bin/dumb-init

COPY docker/apache-cats-foreground /usr/local/bin/
COPY testca/root/ca.crt.pem /usr/local/share/ca-certificates/testca_root.crt
COPY testca/class3/ca.crt.pem /usr/local/share/ca-certificates/testca_class3.crt
COPY testca/certs/cats.cacert.localhost.crt.pem /etc/apache2/ssl/certs/
COPY testca/certs/cats.cacert.localhost.key.pem /etc/apache2/ssl/private/
COPY testca/certs/cachain.crt.pem /etc/apache2/ssl/certs/combined.crt
COPY testca/class3/ca.crt.pem /etc/apache2/ssl/certs/clientca.crt

COPY docker/apache-cats-virtualhost.conf /etc/apache2/sites-available/cats.cacert.localhost.conf

VOLUME /var/www/cats

RUN a2ensite cats.cacert.localhost ; \
    a2dissite 000-default ; \
    a2enmod headers ; \
    a2enmod rewrite ; \
    a2enmod ssl ; \
    cd /usr/local/share/ca-certificates ; \
    curl --silent --remote-name http://www.cacert.org/certs/root_X0F.crt ; \
    curl --silent --remote-name http://www.cacert.org/certs/class3_X0E.crt ; \
    update-ca-certificates

EXPOSE 443

ENTRYPOINT ["/usr/local/bin/dumb-init", "--"]
CMD ["/usr/local/bin/apache-cats-foreground"]
