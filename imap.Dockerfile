FROM debian:buster

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
    apt-get install -y --no-install-recommends \
    ca-certificates \
    curl \
    dovecot-imapd \
    psmisc \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

EXPOSE 25

COPY docker/start-postfix.sh /usr/local