FROM debian:buster

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
    apt-get install -y --no-install-recommends \
    ca-certificates \
    curl \
    dumb-init \
    mutt \
    postfix \
    postfix-pcre \
    psmisc \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

EXPOSE 25

RUN adduser --uid 1000 --gecos "catchall mailbox" --disabled-password catchall ; \
    postconf home_mailbox=Maildir/ ; \
    postconf maillog_file=/dev/stdout ; \
    postconf mailbox_command= ; \
    postconf virtual_alias_maps=pcre:/etc/postfix/virtual ; \
    echo '/.*/ catchall' > /etc/postfix/virtual

VOLUME /home/catchall/Maildir

COPY docker/run-postfix /usr/local/bin/run-postfix

CMD ["dumb-init", "/usr/local/bin/run-postfix"]