FROM debian:buster

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
    apt-get install -y --no-install-recommends \
    ca-certificates \
    curl \
    dovecot-imapd \
    dumb-init \
    mutt \
    psmisc \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

EXPOSE 143

RUN adduser --uid 1000 --gecos "catchall mailbox" --disabled-password catchall
COPY docker/imap_auth.conf /etc/dovecot/conf.d/10-auth.conf
COPY docker/imap_plain_userdb.conf /etc/dovecot/conf.d/auth-plain-userdb.conf.ext

VOLUME /home/catchall/Maildir

COPY docker/run-dovecot /usr/local/bin/run-dovecot

CMD ["dumb-init", "/usr/local/bin/run-dovecot"]