FROM mariadb:focal

COPY docker/initdb.sh /docker-entrypoint-initdb.d/initdb.sh
COPY cacert-software/scripts/db_migrations/*.sh /db_migrations/
COPY cacert-mgr/dbadm/ca_mgr.mysql /mgr_dbadm/
COPY cacert-cats/database/*.sql /cats_db/