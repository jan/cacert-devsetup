#!/bin/sh

set -eux

mysql -h localhost -u root "-p$MYSQL_ROOT_PASSWORD" <<-EOF
CREATE database cacert CHARSET latin1 COLLATE latin1_swedish_ci;
CREATE database $MYSQL_CATS_DATABASE CHARSET latin1 COLLATE latin1_swedish_ci;
CREATE database mgr CHARSET utf8 COLLATE utf8_unicode_ci;
EOF

for script in /db_migrations/*.sh; do
  sh "$script" -h localhost -u root "-p$MYSQL_ROOT_PASSWORD" cacert
done

mysql -h localhost -u root "-p$MYSQL_ROOT_PASSWORD" mgr </mgr_dbadm/ca_mgr.mysql

mysql -h localhost -u root "-p$MYSQL_ROOT_PASSWORD" "${MYSQL_CATS_DATABASE}" </cats_db/create_db.sql
mysql -h localhost -u root "-p$MYSQL_ROOT_PASSWORD" "${MYSQL_CATS_DATABASE}" </cats_db/update1.sql
mysql -h localhost -u root "-p$MYSQL_ROOT_PASSWORD" "${MYSQL_CATS_DATABASE}" </cats_db/update2.sql
mysql -h localhost -u root "-p$MYSQL_ROOT_PASSWORD" "${MYSQL_CATS_DATABASE}" </cats_db/sample_test.sql

mysql -h localhost -u root "-p$MYSQL_ROOT_PASSWORD" cacert <<-'EOF'
INSERT INTO languages (locale, en_co, en_lang, country, lang)
VALUES  ('sq_AL', 'Albania', 'Albanian', 'Shqip&euml;ria', 'shqipe'),
        ('ar_DZ', 'Algeria', 'Arabic', '&#65198;&#65164;&#65166;&#65200;&#65184;&#65248;&#65165;', '&#65172;&#65268;&#65168;&#65198;&#65228;&#65248;&#65165;'),
        ('ar_AA', 'Arabic Speaking', 'Arabic', '&#65172;&#65268;&#65168;&#65198;&#65228;&#65248;&#65165;', '&#65172;&#65268;&#65168;&#65198;&#65228;&#65248;&#65165;'),
        ('es_AR', 'Argentina', 'Spanish', 'Argentina', 'Espa&ntilde;ol'),
        ('en_AU', 'Australia', 'English', 'Australia', 'English'),
        ('de_AT', 'Austria', 'German', '&Ouml;sterreich', 'Deutsch'),
        ('ar_BH', 'Bahrain', 'Arabic', '&#65254;&#65268;&#65198;&#65188;&#65168;&#65248;&#65165;', '&#65172;&#65268;&#65168;&#65198;&#65228;&#65248;&#65165;'),
        ('be_BY', 'Belarus', 'Belarusian', '&#1041;&#1077;&#1083;&#1072;&#1088;&#1091;&#1089;&#1100;', '&#1073;&#1077;&#1083;&#1072;&#1088;&#1091;&#1089;&#1082;&#1080;'),
        ('nl_BE', 'Belgium', 'Dutch', 'Belgi&euml;', 'Nederlands'),
        ('fr_BE', 'Belgium', 'French', 'Belgique', 'fran&ccedil;ais'),
        ('es_BO', 'Bolivia', 'Spanish', 'Bolivia', 'Espa&ntilde;ol'),
        ('sh_BA', 'Bosnia Herzogovina', 'Serbo-Croatian', 'Bosnia Herzogovina', 'Serbo-Croatian'),
        ('pt_BR', 'Brazil', 'Portuguese', 'Brasil', 'Portugu&ecirc;s'),
        ('bg_BG', 'Bulgaria', 'Bulgarian', '&#1041;&#1098;&#1083;&#1075;&#1072;&#1088;&#1080;&#1103;', '&#1073;&#1098;&#1083;&#1075;&#1072;&#1088;&#1089;&#1082;&#1080;'),
        ('en_CA', 'Canada', 'English', 'Canada', 'English'),
        ('fr_CA', 'Canada', 'French', 'Canada', 'fran&ccedil;ais'),
        ('es_CL', 'Chile', 'Spanish', 'Chile', 'Espa&ntilde;ol'),
        ('es_CO', 'Colombia', 'Spanish', 'Colombia', 'Espa&ntilde;ol'),
        ('es_CR', 'Costa Rica', 'Spanish', 'Costa Rica', 'Espa&ntilde;ol'),
        ('hr_HR', 'Croatia', 'Croatian', 'Hrvatska', 'hrvatski'),
        ('cs_CZ', 'Czech Republic', 'Czech', '&#268;esk&aacute; republika', '&#269;e&scaron;tina'),
        ('da_DK', 'Denmark', 'Danish', 'Danmark', 'dansk'),
        ('es_DO', 'Dominican Republic', 'Spanish', 'Rep&uacute;blica Dominicana', 'Espa&ntilde;ol'),
        ('es_EC', 'Ecuador', 'Spanish', 'Ecuador', 'Espa&ntilde;ol'),
        ('ar_EG', 'Egypt', 'Arabic', '&#65198;&#65212;&#65251;', '&#65172;&#65268;&#65168;&#65198;&#65228;&#65248;&#65165;'),
        ('es_SV', 'El Salvador', 'Spanish', 'El Salvador', 'Espa&ntilde;ol'),
        ('et_EE', 'Estonia', 'Estonian', 'Eesti', 'eesti'),
        ('mk_MK', 'FYR Macedonia', 'Macedonian', 'FYR Macedonia', 'Macedonian'),
        ('fi_FI', 'Finland', 'Finnish', 'Suomi', 'suomi'),
        ('sv_FI', 'Finland', 'Swedish', 'Finland', 'svenska'),
        ('fr_FR', 'France', 'French', 'France', 'fran&ccedil;ais'),
        ('de_DE', 'Germany', 'German', 'Deutschland', 'Deutsch'),
        ('el_GR', 'Greece', 'Greek', '&Epsilon;&lambda;&lambda;&#940;&delta;&alpha;', '&epsilon;&lambda;&lambda;&eta;&nu;&iota;&kappa;&#940;'),
        ('es_GT', 'Guatemala', 'Spanish', 'Guatemala', 'Espa&ntilde;ol'),
        ('es_HN', 'Honduras', 'Spanish', 'Honduras', 'Espa&ntilde;ol'),
        ('zh_HK', 'Hong Kong', 'Chinese', '&#39321;&#28207;', '&#20013;&#25991;'),
        ('hu_HU', 'Hungary', 'Hungarian', 'Magyarorsz&aacute;g', 'magyar'),
        ('is_IS', 'Iceland', 'Icelandic', '&Iacute;sland', '&iacute;slenska'),
        ('in_ID', 'Indonesia', 'Indonesian', 'Indonesia', 'Bahasa Indonesia'),
        ('fa_IR', 'Iran', 'Farsi', 'Iran', '&#65264;&#65204;&#65198;&#65166;&#65235;'),
        ('en_IE', 'Ireland', 'English', 'Ireland', 'English'),
        ('he_IL', 'Israel', 'Hebrew', '&#1500;&#1488;&#1512;&#1513;&#1497;', '&#1514;&#1497;&#1512;&#1489;&#1506;'),
        ('iw_IL', 'Israel', 'Hebrew', '&#1500;&#1488;&#1512;&#1513;&#1497;', '&#1514;&#1497;&#1512;&#1489;&#1506;'),
        ('it_IT', 'Italy', 'Italian', 'Italia', 'italiano'),
        ('ja_JP', 'Japan', 'Japanese', '&#26085;&#26412;', '&#26085;&#26412;&#35486;'),
        ('ar_JO', 'Jordan', 'Arabic', '&#65254;&#65194;&#65198;&#65156;&#65248;&#65165;', '&#65172;&#65268;&#65168;&#65198;&#65228;&#65248;&#65165;'),
        ('ko_KR', 'Korea', 'Korean', '&#45824;&#54620;&#48124;&#44397;', '&#54620;&#44397;&#50612;'),
        ('ar_KW', 'Kuwait', 'Arabic', '&#65174;&#65268;&#65262;&#65244;&#65248;&#65165;', '&#65172;&#65268;&#65168;&#65198;&#65228;&#65248;&#65165;'),
        ('es_LA', 'Latin America', 'Spanish', 'Am&eacute;rica latina', 'Espa&ntilde;ol'),
        ('lv_LV', 'Latvia', 'Latvian', 'Latvija', 'latvie&scaron;u'),
        ('ar_LB', 'Lebanon', 'Arabic', '&#65254;&#65166;&#65256;&#65168;&#65247;', '&#65172;&#65268;&#65168;&#65198;&#65228;&#65248;&#65165;'),
        ('de_LI', 'Liechtenstein', 'German', 'Liechtenstein', 'Deutsch'),
        ('lt_LT', 'Lithuania', 'Lithuanian', 'Lietuva', 'lietuvi&#371;'),
        ('fr_LU', 'Luxembourg', 'French', 'Luxembourg', 'fran&ccedil;ais'),
        ('de_LU', 'Luxembourg', 'German', 'Luxemburg', 'Deutsch'),
        ('es_MX', 'Mexico', 'Spanish', 'M&eacute;xico', 'Espa&ntilde;ol'),
        ('ar_MA', 'Morocco', 'Arabic', '&#65172;&#65268;&#65168;&#65198;&#65232;&#65252;&#65248;&#65165; &#65172;&#65244;&#65248;&#65252;&#65252;&#65248;&#65165;', '&#65172;&#65268;&#65168;&#65198;&#65228;&#65248;&#65165;'),
        ('nl_NL', 'Netherlands', 'Dutch', 'Nederland', 'Nederlands'),
        ('en_NZ', 'New Zealand', 'English', 'New Zealand', 'English'),
        ('es_NI', 'Nicaragua', 'Spanish', 'Nicar&aacute;gua', 'Espa&ntilde;ol'),
        ('no_NO', 'Norway', 'Norwegian', 'Norge', 'bokm&aring;l'),
        ('ar_OM', 'Oman', 'Arabic', '&#65254;&#65166;&#65252;&#65227;', '&#65172;&#65268;&#65168;&#65198;&#65228;&#65248;&#65165;'),
        ('es_PA', 'Panama', 'Spanish', 'Panam&aacute;', 'Espa&ntilde;ol'),
        ('es_PY', 'Paraguay', 'Spanish', 'Paraguay', 'Espa&ntilde;ol'),
        ('zh_CN', 'People''s Republic of China', 'Chinese', '&#20013;&#21326;&#20154;&#27665;&#20849;&#21644;&#22269;', '&#20013;&#25991;'),
        ('es_PE', 'Peru', 'Spanish', 'Per&uacute;', 'Espa&ntilde;ol'),
        ('pl_PL', 'Poland', 'Polish', 'Polska', 'polski'),
        ('pt_PT', 'Portugal', 'Portuguese', 'Portugal', 'portugu&ecirc;s'),
        ('ar_QA', 'Qatar', 'Arabic', '&#65198;&#65220;&#65239;', '&#65172;&#65268;&#65168;&#65198;&#65228;&#65248;&#65165;'),
        ('ro_RO', 'Romania', 'Romanian', 'Rom&acirc;nia', 'rom&acirc;n&#259;'),
        ('ru_RU', 'Russia', 'Russian', '&#1056;&#1086;&#1089;&#1089;&#1080;&#1103;', '&#1088;&#1091;&#1089;&#1089;&#1082;&#1080;&#1081;'),
        ('ar_SA', 'Saudi Arabia', 'Arabic', '&#65172;&#65268;&#65194;&#65262;&#65228;&#65204;&#65248;&#65165; &#65172;&#65268;&#65168;&#65198;&#65228;&#65248;&#65165; &#65172;&#65244;&#65248;&#65252;&#65252;&#65248;&#65165;', '&#65172;&#65268;&#65168;&#65198;&#65228;&#65248;&#65165;'),
        ('hr_SP', 'Serbia', 'Romanian', 'Srbija', 'rom&acirc;n&#259;'),
        ('sr_SP', 'Serbia', 'Serbian (Cyrillic)', '&#1032;&#1091;&#1075;&#1086;&#1089;&#1083;&#1072;&#1074;&#1080;&#1112;&#1072;', '&#1089;&#1088;&#1087;&#1089;&#1082;&#1080;'),
        ('zh_SG', 'Singapore', 'Chinese', '&#26032;&#21152;&#22369;', '&#20013;&#25991;'),
        ('sk_SK', 'Slovakia', 'Slovak', 'Slovensk&aacute; republika', 'sloven&#269;ina'),
        ('sl_SI', 'Slovenia', 'Slovene', 'Slovenija', 'slovenski'),
        ('en_ZA', 'South Africa', 'English', 'South Africa', 'English'),
        ('eu_ES', 'Spain', 'Basque', 'Espainia', 'Euskara'),
        ('ca_ES', 'Spain', 'Catalan', 'Espanya', 'catal&agrave;'),
        ('es_ES', 'Spain', 'Spanish', 'Espa&ntilde;a', 'Espa&ntilde;ol'),
        ('sv_SE', 'Sweden', 'Swedish', 'Sverige', 'svenska'),
        ('fr_CH', 'Switzerland', 'French', 'Suisse', 'fran&ccedil;ais'),
        ('de_CH', 'Switzerland', 'German', 'Schweiz', 'Deutsch'),
        ('it_CH', 'Switzerland', 'Italian', 'Svizzera', 'italiano'),
        ('ar_SY', 'Syria', 'Arabic', '&#65166;&#65268;&#65198;&#65262;&#65203;', '&#65172;&#65268;&#65168;&#65198;&#65228;&#65248;&#65165;'),
        ('zh_TW', 'Taiwan', 'Chinese', '&#20013;&#33775;&#27665;&#22283;', '&#20013;&#25991;'),
        ('th_TH', 'Thailand', 'Thai', '&#3652;&#3607;&#3618;', '&#3652;&#3607;&#3618;'),
        ('ar_TN', 'Tunisia', 'Arabic', '&#65202;&#65256;&#65262;&#65175;', '&#65172;&#65268;&#65168;&#65198;&#65228;&#65248;&#65165;'),
        ('tr_TR', 'Turkey', 'Turkish', 'T&uuml;rkiye', 'T&uuml;rk&ccedil;e'),
        ('ar_UA', 'U.A.E.', 'Arabic', '&#65172;&#65194;&#65188;&#65176;&#65252;&#65248;&#65165; &#65172;&#65268;&#65168;&#65198;&#65228;&#65248;&#65165; &#65174;&#65166;&#65198;&#65166;&#65252;&#65160;&#65248;&#65165;', '&#65172;&#65268;&#65168;&#65198;&#65228;&#65248;&#65165;'),
        ('uk_UA', 'Ukraine', 'Ukrainian', '&#1059;&#1082;&#1088;&#1072;&#1111;&#1085;&#1072;', '&#1091;&#1082;&#1088;&#1072;&#1111;&#1085;&#1100;&#1089;&#1082;&#1072;'),
        ('en_GB', 'United Kingdom', 'English', 'United Kingdom', 'English'),
        ('en_US', 'United States', 'English', 'United States', 'English'),
        ('es_US', 'United States', 'Spanish', 'Estados Unidos', 'Espa&ntilde;ol'),
        ('es_UY', 'Uruguay', 'Spanish', 'Uruguay', 'Espa&ntilde;ol'),
        ('es_VE', 'Venezuela', 'Spanish', 'Venezuela', 'Espa&ntilde;ol'),
        ('vi_VN', 'Vietnam', 'Vietnamese', 'Vi&#7879;t Nam', 'Ti&#7875;ng Vi&#7879;t'),
        ('ar_YE', 'Yemen', 'Arabic', '&#65254;&#65252;&#65268;&#65248;&#65165;', '&#65172;&#65268;&#65168;&#65198;&#65228;&#65248;&#65165;');
EOF

mysql -h localhost -u root "-p$MYSQL_ROOT_PASSWORD" <<-EOF
CREATE USER $MYSQL_WEBDB_USER@'%' IDENTIFIED BY '$MYSQL_WEBDB_PASSWORD';
GRANT CREATE TEMPORARY TABLES ON cacert.* TO $MYSQL_WEBDB_USER@'%';
GRANT SELECT, INSERT, UPDATE, DELETE ON cacert.* TO $MYSQL_WEBDB_USER@'%';

CREATE USER $MYSQL_MGR_USER@'%' IDENTIFIED BY '$MYSQL_MGR_PASSWORD';
GRANT SELECT, INSERT, UPDATE, DELETE ON mgr.* TO $MYSQL_MGR_USER@'%';
GRANT SELECT, INSERT, UPDATE, DELETE ON cacert.* TO $MYSQL_MGR_USER@'%';

CREATE USER $MYSQL_CATS_USER@'%' IDENTIFIED BY '$MYSQL_CATS_PASSWORD';
GRANT SELECT, INSERT, UPDATE, DELETE ON $MYSQL_CATS_DATABASE.* TO $MYSQL_CATS_USER@'%';
EOF
